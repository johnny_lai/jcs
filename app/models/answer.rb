class Answer < ActiveRecord::Base
  attr_accessible :answersData
  attr_accessible :user_id, :post_id
  
  belongs_to :user
  belongs_to :post
end
