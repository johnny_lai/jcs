class User < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_accessible :icon_url
  attr_accessible :email
  attr_accessible :first_name, :last_name, :company, :is_company
  attr_accessible :posts
  attr_accessible :device_token

  has_many :posts
end
