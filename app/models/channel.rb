class Channel < ActiveRecord::Base
  attr_accessible :icon_url
  attr_accessible :summary, :name, :posts

  has_many :posts
end
