class Post < ActiveRecord::Base
  attr_accessible :message
  attr_accessible :form_data
  attr_accessible :channel, :user
  attr_accessible :channel_id, :user_id
  
  attr_accessible :parent_id
  attr_accessible :parent, :children

  belongs_to :parent, class_name: "Post", foreign_key: "parent_id"
  has_many :children, class_name: "Post", foreign_key: "parent_id"
  
  belongs_to :channel
  belongs_to :user
end
