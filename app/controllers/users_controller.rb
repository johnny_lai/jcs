class UsersController < ApplicationController
  # GET /users.json
  def index
    @users = User.all

    render json: @users
  end

  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.json { render json: { user: @user } }
    end
  end

  # POST /users.json
  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save
        format.json { render json: { user: @user }, status: :created, location: @user }
      else
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.json { render json: nil, status: :ok }
      else
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.json { render json: nil, status: :ok }
    end
  end
end
