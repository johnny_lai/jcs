class PostsController < ApplicationController
  # GET /posts
  def index
    posts = Post.includes(:children).all

    render json: posts.to_json(:include => :children)
  end
  
  # GET /posts/1
  def show
    post = Post.includes(:children).find(params[:id])

    render json: post.to_json(:include => :children)
  end

  # POST /posts.json
  def create
    post = Post.new(params[:post])

    if post.save
      render json: post, status: :ok
    else
      render json: post.errors, status: :unprocessable_entity
    end
  end

  # PUT /posts/1.json
  def update
    post = Post.find(params[:id])

    if post.update_attributes(params[:post])
      render json: post, status: :ok
    else
      render json: post.errors, status: :unprocessable_entity
    end
  end

  # DELETE /posts/1.json
  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    respond_to do |format|
      format.json { render json: nil, status: :ok }
    end
  end
end
