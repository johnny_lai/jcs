class ChannelsController < ApplicationController
  # GET /channels.json
  def index
    channels = Channel.includes(:posts).find_all
    render json: channels.to_json(:include => :posts)
  end

  # GET /channels/1.json
  def show
    channel = Channel.includes(:posts).find(params[:id])

    render json: channel.to_json(:include => :posts)
  end

  # POST /channels.json
  def create
    @channel = Channel.new(params[:channel])

    respond_to do |format|
      if @channel.save
        format.json { render json: { channel: @channel}, status: :created, location: @channel }
      else
        format.json { render json: @channel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /channels/1.json
  def update
    @channel = Channel.find(params[:id])

    respond_to do |format|
      if @channel.update_attributes(params[:channel])
        format.json { render json: nil, status: :ok }
      else
        format.json { render json: @channel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /channels/1.json
  def destroy
    @channel = Channel.find(params[:id])
    @channel.destroy

    respond_to do |format|
      format.json { render json: nil, status: :ok }
    end
  end
end
