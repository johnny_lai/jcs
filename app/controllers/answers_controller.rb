class AnswersController < ApplicationController
  # GET /answers
  def index
    answers = Answer.all
    
    render json: answers
  end
  
  # GET /answers/1.json
  def show
    answer = Answer.find(params[:id])
    
    respond_to do |format|
      format.json { render json: { answer: answer } }
    end
  end
  
  # answer /answers.json
  def create
    answer = Answer.new(params[:answer])
    
    respond_to do |format|
      if answer.save
        format.json { render json: { answer: answer }, status: :created, location: answer }
        else
        format.json { render json: answer.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PUT /answers/1.json
  def update
    answer = Answer.find(params[:id])
    
    respond_to do |format|
      if answer.update_attributes(params[:answer])
        format.json { render json: nil, status: :ok }
        else
        format.json { render json: answer.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /answers/1.json
  def destroy
    answer = Answer.find(params[:id])
    answer.destroy
    
    respond_to do |format|
      format.json { render json: nil, status: :ok }
    end
  end
end
