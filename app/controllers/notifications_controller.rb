class NotificationsController < ApplicationController
  # curl -d 'notification[alert]=台北市長宣布立即停班停課&notification[user_id]=1&notification[badge]=1' localhost:3000/notifications
  def create
    np = params[:notification]

    n = Rapns::Notification.new
    n.app = "jute"
    n.device_token = User.find(np[:user_id]).device_token
    n.alert = np[:alert]
    n.badge = np[:badge]
    n.save!

    respond_to do |format|
       format.json {
          render json: {
            ok: true
          }
        }
    end
  end
end
