class PublishersController < ApplicationController
  # GET /publishers.json
  def index
    @publishers = Publisher.all

    respond_to do |format|
      format.json { render json: @publishers }
    end
  end

  # GET /publishers/1.json
  def show
    @user = Publisher.find(params[:id])

    respond_to do |format|
      format.json { render json: @user }
    end
  end

  # POST /publishers.json
  def create
    @user = Publisher.new(params[:publisher])

    respond_to do |format|
      if @user.save
        format.json { render json: @user, status: :created, location: @user }
      else
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /publishers/1.json
  def update
    @user = Publisher.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:publisher])
        format.json { render json: nil, status: :ok }
      else
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /publishers/1.json
  def destroy
    @user = Publisher.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.json { render json: nil, status: :ok }
    end
  end
end
