/*
 *  PullToRefresh.js
 *
 *  ********************************************************************************
 *  TREND MICRO HIGHLY CONFIDENTIAL INFORMATION:
 *  THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF TREND MICRO
 *  INCORPORATED AND MAY BE PROTECTED BY ONE OR MORE PATENTS. USE, DISCLOSURE, OR
 *  REPRODUCTION OF ANY PORTION OF THIS SOFTWARE IS PROHIBITED WITHOUT THE PRIOR
 *  EXPRESS WRITTEN PERMISSION OF TREND MICRO INCORPORATED.
 *  Copyright 2012 Trend Micro Incorporated. All rights reserved as an unpublished work.
 *  ********************************************************************************
 *
 */

/**
 * Renders the pull-to-refresh indicator
 * Rendering is based on https://github.com/Sephiroth87/ODRefreshControl/blob/master/ODRefreshControl/ODRefreshControl.m
 */
App.PullToRefreshIndicator = Ember.View.extend({
    defaultTemplate: Ember.Handlebars.compile("<canvas></canvas>{{#if isLoading}}<div class=\"is-loading\"></div>{{/if}}"),

    canvas: Ember.computed(function() {
        return this.$("canvas");
    }).property(),

    minTopPadding: 9,
    maxTopPadding: 5,
    minTopRadius: 12.5,
    maxTopRadius: 16,

    minBottomPadding: 6.5,
    maxBottomPadding: 10,
    minBottomRadius: 3,
    maxBottomRadius: 16,

    minArrowSize: 2,
    maxArrowSize: 3,
    minArrowRadius: 5,
    maxArrowRadius: 7,
    
    marginTop: 5,
    marginBottom: 5,

    tintFillStyle: "rgba(155, 162, 172, %@)",
    tintStrokeStyle: "rgba(85, 85, 85, 0.5)",
    
    arrowFillStyle: "white",
    
    width: Ember.computed(function(key, value) {
        var canvas = this.get("canvas");
        if (value === undefined) {
            return canvas.width();
        } else {
            canvas[0].setAttribute("width", value);
            canvas.width(value);
            return value;
        }
    }).property().cacheable(),
    height: Ember.computed(function(key, value) {
        var canvas = this.get("canvas");
        if (value === undefined) {
            return canvas.height();
        } else {
            canvas[0].setAttribute("height", value);
            canvas.height(value);
            return value;
        }
    }).property().cacheable(),

    minOffset: Ember.computed(function() {
        return this.get("maxTopPadding") + this.get("maxTopRadius") +
               this.get("maxBottomPadding") + this.get("maxBottomRadius");
    }).property().cacheable(),
    maxOffset: 100,
    offset: 0,
    
    isLoading: false,
    
    lerp: function(a, b, p) {
        return a + (b - a) * p;
    },

    startAnimation: function() {
        if (this.get("isLoading")) {
            this.animateLoading();
        }
    }.observes("isLoading"),
    
    animateLoading: function() {
        var that = this;
        
        var startOffset = this.get("offset");
        var minOffset = this.get("minOffset");
        
        var start = new Date();
        var totalms = 200;
        var animation = function() {
            var now = new Date();
            var ratio = 1 - ((now - start) / totalms);
            if (ratio > 0) {
                var offset = (ratio * (startOffset - minOffset)) + minOffset;
                that.set("offset", offset);
                setTimeout(animation, 50);
            } else {
                that.set("offset", minOffset);
            }
        };
        animation();
    },

    draw: function() {
        var canvas = this.get("canvas");

        var width = this.get("width");

        var ctx = canvas[0].getContext("2d");
        ctx.clearRect(0, 0, canvas.width(), canvas.height());

        var minOffset = this.get("minOffset");
        var maxOffset = this.get("maxOffset");
        var maxDistance = (maxOffset - minOffset);
        
        var offset = this.get("offset");
        var distance = 0;        
        var percentage = 1;
        if (offset > minOffset) {
            distance = (offset - minOffset);
            if (distance > maxDistance) {
                distance = maxDistance;
            }
            percentage = 1 - (distance / maxDistance);
        }

        var currentTopPadding = this.lerp(this.get("minTopPadding"), this.get("maxTopPadding"), percentage);
        var currentTopRadius = this.lerp(this.get("minTopRadius"), this.get("maxTopRadius"), percentage);
        var currentBottomPadding = this.lerp(this.get("minBottomPadding"), this.get("maxBottomPadding"), percentage);
        var currentBottomRadius= this.lerp(this.get("minBottomRadius"), this.get("maxBottomRadius"), percentage);

        var topOriginX = width / 2;
        var topOriginY = currentTopPadding + currentTopRadius;

        var bottomOriginX, bottomOriginY;
        if (distance == 0) {
            bottomOriginX = width / 2;
            bottomOriginY = topOriginY;
        } else {
            bottomOriginX = width / 2;
            bottomOriginY = topOriginY + distance; // - currentBottomPadding - currentBottomRadius;
            if (bottomOriginY < topOriginY) {
                bottomOriginY = topOriginY;
            }
        }

        var alpha = 1.0;
        if (this.get("isLoading")) {
            alpha = 1 - percentage;
            ctx.strokeStyle = "white";
        } else {
            ctx.strokeStyle = this.get("tintStrokeStyle");
        }
        ctx.fillStyle = Ember.String.fmt(this.get("tintFillStyle"), [alpha]);
        ctx.beginPath();
        ctx.moveTo(topOriginX, topOriginY);
        
        // Top semicircle
        ctx.arc(topOriginX, topOriginY, currentTopRadius, 0, Math.PI, true);

        // Left curve
        var leftCp1X = this.lerp(topOriginX - currentTopRadius, bottomOriginX - currentBottomRadius, 0.1);
        var leftCp1Y = this.lerp(topOriginY, bottomOriginY, 0.2);
        var leftCp2X = this.lerp(topOriginX - currentTopRadius, bottomOriginX - currentBottomRadius, 0.9);
        var leftCp2Y = this.lerp(topOriginY, bottomOriginY, 0.2);
        var leftDestinationX = bottomOriginX - currentBottomRadius;
        var leftDestinationY = bottomOriginY;
        ctx.bezierCurveTo(leftCp1X, leftCp1Y, leftCp2X, leftCp2Y, leftDestinationX, leftDestinationY);

        // Bottom semicircle
        ctx.arc(bottomOriginX, bottomOriginY, currentBottomRadius, Math.PI, 0, true);

        // Right curve
        var rightCp2X = this.lerp(topOriginX + currentTopRadius, bottomOriginX + currentBottomRadius, 0.1);
        var rightCp2Y = this.lerp(topOriginY, bottomOriginY, 0.2);
        var rightCp1X = this.lerp(topOriginX + currentTopRadius, bottomOriginX + currentBottomRadius, 0.9);
        var rightCp1Y = this.lerp(topOriginY, bottomOriginY, 0.2);
        var rightDestinationX = topOriginX + currentTopRadius;
        var rightDestinationY = topOriginY;
        ctx.bezierCurveTo(rightCp1X, rightCp1Y, rightCp2X, rightCp2Y, rightDestinationX, rightDestinationY);

        ctx.fill();
        
        
        // Draw arrow
        if (!this.get("isLoading")) {
            var currentArrowSize = this.lerp(this.get("minArrowSize"), this.get("maxArrowSize"), percentage);
            var currentArrowRadius = this.lerp(this.get("minArrowRadius"), this.get("maxArrowRadius"), percentage);
            var arrowBigRadius = currentArrowRadius + (currentArrowSize / 2);
            var arrowSmallRadius = currentArrowRadius - (currentArrowSize / 2);
            
            ctx.fillStyle = this.get("arrowFillStyle");
            ctx.beginPath();
            ctx.arc(topOriginX, topOriginY, arrowBigRadius, 0, 3 * Math.PI / 2, false);
            ctx.lineTo(topOriginX, topOriginY - arrowBigRadius - currentArrowSize);
            ctx.lineTo(topOriginX + (2 * currentArrowSize), topOriginY - arrowBigRadius + (currentArrowSize / 2));
            ctx.lineTo(topOriginX, topOriginY - arrowBigRadius + (2 * currentArrowSize));
            ctx.lineTo(topOriginX, topOriginY - arrowBigRadius + currentArrowSize);
            ctx.arc(topOriginX, topOriginY, arrowSmallRadius, 3 * Math.PI / 2, 0, true);
            ctx.fill();
        }
    }.observes("isLoading", "offset", "width")
});

App.PullToRefreshView = Ember.View.extend(Ember.TargetActionSupport, {
    layoutName: "hmdm/templates/pull_to_refresh",
    
    attributeBindings: ["data-role"],
    
    hasTriggered: false,
    
    pullToRefreshElement: Ember.computed(function() {
        return this.$()[0];
    }).property(),
    
    triggerPullDistance: Ember.computed(function() {
        return this.getPath("indicator.maxOffset") + 5;
    }).property("indicator.maxOffset").cacheable(),
    
    updateIndicator: function(height) {
        var indicator = this.get("indicator");
        var minOffset = indicator.get("minOffset");
        if (height < minOffset) {
            indicator.$().css("top", (height - minOffset) + "px");
            indicator.set("offset", 0);
        } else {
            indicator.$().css("top", "0px");
            indicator.set("offset", height);
        }
    },
    
    /**
     * This function should be called once during didInsertElement
     */
    pullToRefreshOnDidInsertElement: function() {
        var that = this;
        that.setPath("indicator.width", that.$().outerWidth(true));
    
        $(window).resize(function() {
            that.setPath("indicator.width", that.$().outerWidth(true));
        });
        
        that.set("iScroll", new iScroll(that.get("pullToRefreshElement"), {
            scrollerChildIndex: 1,
            useTransition: true,
            topOffset: 0,
            onScrollStart: function(e) {
                that.set("isScrolling", true);
            },
            onScrollMove: function(e) {
                var trigger_distance = that.getPath("triggerPullDistance");
                if (this.y > trigger_distance) {
                    if(!that.get("hasTriggered")) {
                        that.set("hasTriggered", true);
                        that.setPath("indicator.isLoading", true);
                        this.minScrollY = that.getPath("indicator.minOffset");
                        that.fire('refresh');
                    }
                } else {
                    that.updateIndicator(this.y);
                }
            },
            onBeforeScrollEnd: function(e) {
                if (!that.getPath("indicator.isLoading")) {
                    var startOffset = that.getPath("indicator.offset");
                    var minOffset = this.minScrollY;
                    
                    var start = new Date();
                    var totalms = 200;
                    var animation = function() {
                        var now = new Date();
                        var ratio = 1 - ((now - start) / totalms);
                        if (ratio > 0) {
                            var offset = (ratio * (startOffset - minOffset)) + minOffset;
                            that.updateIndicator(offset);
                            setTimeout(animation, 50);
                        } else {
                            that.updateIndicator( minOffset);
                        }
                    };
                    animation();
                }
            },
            onScrollEnd: function(e) {
                that.set("isScrolling", false);
            }
        }));
    },
    
    refresh: function() {
        if(!this.triggerAction()) {
            this.reset();
        }
    },
    
    /**
     * Call this function to reset the view's state
     */
    reset: function() {
        var iscroll = this.get("iScroll");
        iscroll.minScrollY = 0;
        this.updateIndicator(0);
        this.set("hasTriggered", false);
        this.setPath("indicator.isLoading", false);
        if (!this.get("isScrolling")) {
            iscroll.refresh();
        }
    },

    didInsertElement: function() {
        this._super();
        this.pullToRefreshOnDidInsertElement();
    }
});

App.BouncyScrollView = Ember.View.extend({
    attributeBindings: ["data-role"],
    defaultLayout: Ember.Handlebars.compile("<div>{{yield}}</div>"),

    pullToRefreshElement: Ember.computed(function() {
        return this.$()[0];
    }).property(),
    
    /**
     * This function should be called once during didInsertElement
     */
    onDidInsertElement: function() {
        var that = this;
        // view could be destroyed by the time we get here
        if(!that.get("isDestroyed")) {
            that.set("iScroll", new iScroll(that.get("pullToRefreshElement"), {
                useTransition: true,
                topOffset: 0
            }));
        }
    },

    didInsertElement: function() {
        this._super();
        this.onDidInsertElement();
    }
});
