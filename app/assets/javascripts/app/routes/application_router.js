App.Router = Ember.Router.extend({
  root: Ember.Route.extend({
    index: Ember.Route.extend({
      route: '/',
      enter: function(router) {
        Ember.run.next(function() {
          router.transitionTo('posts.index');
        });
      }
    }),

    posts: Ember.Route.extend({
      route: '/posts',
      index: Ember.Route.extend({
        route: '/',
        connectOutlets: function(router, context) {
          router.get('applicationController').connectOutlet('posts');
        }
      })
    }),

    channels: Ember.Route.extend({
      route: '/channels',
      index: Ember.Route.extend({
        route: '/'
      })
    }),

    channel: Ember.Route.extend({
      route: '/channels/:channel_id',
      connectOutlets: function(router, channel) {
        // channel will become the content of the TimelineController instance
        router.get('applicationController').connectOutlet('timeline', channel);
        router.set('applicationController.active' ,false);
      }
    }),
  

    doPosts: function(router, event) {
      router.transitionTo('posts.index');
    },
    doChannels: function(router, event) {
      router.transitionTo('channels.index');
    },

    showChannel: Ember.Router.transitionTo("channel"),

    toggleSideMenu: function(router, event) {
      router.toggleProperty("applicationController.active");
    }
  })
});
