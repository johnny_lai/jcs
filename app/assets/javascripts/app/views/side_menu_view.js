App.SideMenuView = Ember.View.extend({
	templateName: "app/templates/side_menu",
	classNames: ["side-menu"],

  channelSelector: Ember.View.extend({
    tagName: "li",
    classNames: ["channel"],
    classNameBindings: ["active"],
    active: function() {
      return App.get("router.location.lastSetURL") === "/channels/" + this.get("content.id");
    }.property("content.id", "App.router.location.lastSetURL").cacheable()
  })
});