App.Post = DS.Model.extend({
  format: DS.attr('string'),
  message: DS.attr('string'),
  channel: DS.belongsTo('App.Channel'),
  user: DS.belongsTo('App.User')
});