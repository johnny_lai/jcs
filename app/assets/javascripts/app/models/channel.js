App.Channel = DS.Model.extend({
  name: DS.attr('string'),
  description: DS.attr('string'),
  posts: DS.hasMany('App.Post')
}).reopenClass({
  url: 'channel'
});
