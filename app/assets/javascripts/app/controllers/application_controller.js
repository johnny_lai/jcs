App.ApplicationController = Ember.Controller.extend({
  active: false,

  channels: Ember.ArrayController.create({
    content: App.store.findAll(App.Channel)
  })
});