App.TimelineController = Ember.Controller.extend({
  content: null,
  posts: function() {
    var id = this.get("content.id");
    return Ember.ArrayController.create({
      content: App.store.find(App.Post, { toId : id })
    })
  }.property("content.id").cacheable()
});