# encoding: utf-8

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

johnny_lai =
User.create :icon_url => "https://s3-ap-northeast-1.amazonaws.com/jute/avatars/johnny_lai.png",
            :first_name => "Johnny",
            :last_name => "Lai",
            :is_company => false,
            :email => "johnny_lai@trend.com.tw",
            :device_token => "89471a28ed530a925f94e3d112c4d6815f120c6d09d6427acdbb2ea2c4eb7004"

chris_chen =
User.create :icon_url => "https://s3-ap-northeast-1.amazonaws.com/jute/avatars/chris_chen.png",
            :first_name => "Chris",
            :last_name => "Chen",
            :email => "chris_hy_chen@trend.com.tw",
            :is_company => false

randy_lien =            
User.create :icon_url => "https://s3-ap-northeast-1.amazonaws.com/jute/avatars/randy_lien.png",
            :first_name => "Randy",
            :last_name => "Lien",
            :email => "randy_lien@trend.com.tw",
            :is_company => false,
            :device_token => "d6ffa478b75888e12f98fce6091fc0cb121a75b3f3ad39d9378cc1722250f7ea"

taipei_gov =
User.create :icon_url => "https://s3-ap-northeast-1.amazonaws.com/jute/channels/government.png",
            :first_name => "",
            :last_name => "",
            :is_company => true,
            :company => "Taipei Government"

c1 = Channel.create :icon_url => "https://s3-ap-northeast-1.amazonaws.com/jute/channels/government.png",
                    :name => "Taipei Government",
                    :summary => "This channel provide public services information from the Taipei Municipal Government."
c1.posts.create(:message => "Work and class has been cancelled due to Typhoon Saola.",
                :user => taipei_gov);

c2 = Channel.create :icon_url => "https://s3-ap-northeast-1.amazonaws.com/jute/channels/expenditure.png",
                    :name => "Expenditures",
                    :summary => "Let's keep our expenditure or expenses in this channel."

expenditure =
c2.posts.create(:message => "Expenditure Form",
                :user => chris_chen,
                :form_data => {
                    "questions" => [
                        { "type" => "select", "key" => "type", "title" => "Type", "defaultValue" => "", "choices" => [ "Food", "Electronics", "Stationery", "Clothing", "Services" ] },
                        { "type" => "text", "key" => "item", "title" => "Item", "defaultValue" => "" },
                        { "type" => "integer", "key" => "price", "title" => "Price", "defaultValue" => "" }
                    ]
                }.to_json);
expenditure.children.create(:message => "Lunch",
                :user => randy_lien,
                :form_data => {
                    "answers" => {
                        "type" => "Food",
                        "item" => "Lunch",
                        "price" => 120
                    }
                }.to_json);

c3 = Channel.create :icon_url => "https://s3-ap-northeast-1.amazonaws.com/jute/channels/feedback.png",
                    :name => "Feedback",
                    :summary => "Provide your feedback here to help make Jute better!"

c3.posts.create(:message => "October Feedback!",
                :user => chris_chen,
                :form_data => {
                    "questions" => [
                        { "type" => "boolean", "key" => "use_micro", "title" => "Twitter/other services?", "defaultValue" => false },
                        { "type" => "boolean", "key" => "use_mobile_internal", "title" => "In-house mobile apps?", "defaultValue" => false },
                        { "type" => "boolean", "key" => "use_collaborative", "title" => "Collaborative apps?", "defaultValue" => false },
                        { "type" => "select", "key" => "os", "title" => "Most-used mobile OS?", "defaultValue" => "None", "choices" => [ "iOS", "Android", "Windows Phone 7/8", "Other", "None" ] },
                        { "type" => "select", "key" => "best", "title" => "Most enticing feature?", "defaultValue" => "", "choices" => [ "Public Services Channels", "3rd-Party Integration", "Comments", "Forms",  "Reports", "None" ] }
                    ]
                }.to_json);

