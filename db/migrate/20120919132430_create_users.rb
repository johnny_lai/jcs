class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.string :icon_url
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :company
      t.boolean :is_company
      t.string :device_token
      
      t.timestamps
    end
  end

  def down
    drop_table :users
  end
end
