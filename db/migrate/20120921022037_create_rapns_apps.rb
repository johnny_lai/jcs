class CreateRapnsApps < ActiveRecord::Migration
  def self.up
    create_table :rapns_apps do |t|
      t.string    :key,             :null => false
      t.string    :environment,     :null => false
      t.text      :certificate,     :null => false
      t.string    :password,        :null => true
      t.integer   :connections,     :null => false, :default => 1
      t.timestamps
    end

    app = Rapns::App.new
    app.key = "jute"
    app.certificate = File.read("./config/cert.pem")
    app.environment = "development"
    app.password = "12qwaszx"
    app.connections = 1
    app.save!
  end

  def self.down
    drop_table :rapns_apps
  end
end
