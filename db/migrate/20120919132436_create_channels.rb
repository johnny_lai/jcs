class CreateChannels < ActiveRecord::Migration
  def up
    create_table :channels do |t|
      t.string :icon_url
      t.string :name
      t.string :summary
      
      t.timestamps
    end
  end

  def down
    drop_table :channels
  end
end
