class CreatePublishers < ActiveRecord::Migration
  def up
    create_table :publishers do |t|
      t.string :type
      t.string :email
      
      t.string :name
      t.text :description
      
      t.string :first_name
      t.string :last_name
      t.string :company
      t.boolean :is_company
      
      t.timestamps
    end
  end

  def down
    drop_table :publishers
  end
end
