class CreatePosts < ActiveRecord::Migration
  def up
    create_table :posts do |t|
      t.integer :channel_id
      t.integer :user_id
      t.integer :parent_id

      t.text :message
      t.text :form_data

      t.timestamps
    end
  end

  def down
    drop_table :posts
  end
end
